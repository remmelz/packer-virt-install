#!/bin/bash

_disk=${1}

# Install default packages
pacman --noconfirm -Sy \
  networkmanager lvm2 \
  sudo which python man \
  bash-completion openssh

# Enable services
systemctl enable NetworkManager
systemctl enable sshd

# SSH config
sed -i '/PermitRootLogin/aPermitRootLogin yes' /etc/ssh/sshd_config
sed -i '/PermitRootLogin/aPasswordAuthentication yes' /etc/ssh/sshd_config

# Set timezone
ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
hwclock --systohc

# Set locale
sed -i 's/^#en_US.UTF/en_US.UTF/g' /etc/locale.gen
locale-gen

# Set hostname
echo 'archlinux.libvirt.lan' > /etc/hostname

# Create new initramfs
sed -i 's/block filesystems/block lvm2 filesystems/g' /etc/mkinitcpio.conf
mkinitcpio -P

# Install GRUB
pacman --noconfirm -Sy grub efibootmgr
grub-install \
  --target=x86_64-efi \
  --efi-directory=esp \
  --bootloader-id=GRUB

sed -i 's/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=1/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
grub-install --target=i386-pc /dev/${_disk}

# Set the password
echo 'root:changeme' | chpasswd


