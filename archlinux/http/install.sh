#!/bin/bash

set -x

# Download other files
curl -sO http://${_http}/chroot.sh

# Make script executable
chmod a+x *.sh

# Set ntp
timedatectl set-ntp true

# Retrieve the disk name
_disk=`lsblk \
  | egrep 'G .*disk' \
  | awk -F' ' '{print $1}'`

# Format the disk
fdisk /dev/${_disk} <<EOL
n
p
1
1


p
w
EOL

# Create LVM partitions
sleep 1
pvcreate /dev/${_disk}1
vgcreate sysvg /dev/${_disk}1
lvcreate -n swaplv -L 1G sysvg
lvcreate -n rootlv -l 100%FREE sysvg

# Format the partitions
mkfs.ext4 /dev/sysvg/rootlv
mkswap /dev/sysvg/swaplv

# Mount the partitions
mount /dev/sysvg/rootlv /mnt
swapon /dev/sysvg/swaplv

# Refresh keys
pacman --noconfirm -Sy archlinux-keyring

# Install base packages
pacstrap /mnt base linux linux-firmware

# Generate fstab
genfstab -U /mnt >> /mnt/etc/fstab

# Execute script in chroot mode
cp chroot.sh /mnt/
arch-chroot /mnt /chroot.sh ${_disk}

# Copy default vim config
cp vimrc /mnt/etc/vimrc

# Shutdown
shutdown -h now


