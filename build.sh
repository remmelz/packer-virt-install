#!/bin/bash
LIBVIRT_DEFAULT_URI="qemu:///system"

. ./build.cfg

#####################################################
# Functions
#####################################################

function waitSSH {

  ipaddr=$1

  c=1
  while true; do

    nc -z -w1 ${ipaddr} 22

    [[ $? == 0 ]] && break
    [[ ${c} -eq 10 ]] && break

    sleep 3
    let c=${c}+1
 done
}

function retrieveIP {

  resource=$1

  macaddr=`sudo virsh dumpxml ${resource} \
    | grep 'mac addr' \
    | awk -F"'" '{print $2}'`

  c=1
  while true; do

    if [[ -n ${macaddr} ]]; then
      ipaddr=`sudo virsh net-dhcp-leases default \
        | grep ${macaddr} \
        | awk -F' ' '{print $5}' \
        | awk -F'/' '{print $1}'`
    fi

    [[ -n ${ipaddr} ]] && break
    [[ ${c} -eq 10 ]] && break

    sleep 3
    let c=${c}+1
  done

  echo ${ipaddr}
}


#####################################################
# Sanity check
#####################################################

which packer \
      sshpass \
      pwgen \
      ansible-playbook \
      qemu-img \
      qemu-system-x86_64 \
      virt-install \
      virsh > /dev/null

if [[ $? != 0 ]]; then
  echo 'Error: one or more required dependencies are missing.'
  exit 1
fi


#####################################################
# Precheck
#####################################################

_template=$1

if [[ -z ${_template} ]]; then
  echo "Usage: $0 <packer_template>"
  exit 1
fi

_distro=`dirname ${_template}`
_distro=`echo ${_distro} | sed 's/\.//g'`
_distro=`echo ${_distro} | sed 's|/||g'`
_image=`basename ${_template}`
_image=`echo ${_image} | sed 's/\.json/\.qcow2/g'`

if [[ ! -f ${_template} ]]; then
  echo "Error: Could not find ${_template}"
  exit 1
fi

# Test if images folder is writable
touch ${_libvirt}/test || exit 1
rm ${_libvirt}/test

[[ ! -d ./${_distro}/images ]] \
  && mkdir ./${_distro}/images


#####################################################
# Run Packer
#####################################################

echo -e "\e[96m[Running Packer]\e[39m"

if [[ ! -f ./${_distro}/images/${_image} ]]; then

  packer build ${_template}
  [[ $? -ne 0 ]] && exit 

  mv ./output/* ./${_distro}/images/
  rmdir ./output || exit 1
fi

if [[ ! -f ./${_distro}/images/${_image} ]]; then
  echo "Error: No images found"
  exit 1
else
  echo "Found image ${_image}..."
  echo
fi


#####################################################
# OS settings
#####################################################

_os=${_distro}
_python="/usr/bin/python"
_shell='/bin/bash'

if [[ ${_distro} == 'rockylinux' ]]; then
  _os='rhel8-unknown'
  _python='/usr/bin/python3'
fi

if [[ ${_distro} == 'freebsd' ]]; then
  _os='freebsd12.2'
  _python='/usr/local/bin/python'
  _shell='/usr/local/bin/bash'
fi

if [[ ${_distro} == 'debian' ]]; then
  _os='debiantesting'
  _python='/usr/bin/python3'
fi

#####################################################
# Install Image
#####################################################

echo -e "\e[96m[Creating Virtual Host]\e[39m"

_rand=`pwgen -1A ${_randlen}`

cp -v ./${_distro}/images/${_image} ${_libvirt}/${_distro}-${_rand}.qcow2

sudo virt-install \
  --import \
  --name ${_distro}-${_rand} \
  --memory ${_memory} \
  --vcpus ${_vcpu} \
  --disk ${_libvirt}/${_distro}-${_rand}.qcow2 \
  --os-variant ${_os} \
  --network ${_network} \
  --autoconsole none

if [[ $? -gt 0 ]]; then
  echo "Error: something went wrong. Aborting.."
  exit 1
fi

echo
sleep 3


#####################################################
# Create inventory file
#####################################################

echo -e "\e[96m[Creating Inventory File]\e[39m"

_ipaddr=`retrieveIP ${_distro}-${_rand}`
_inventory="./inventory/${_distro}-${_rand}.ini"

if [[ -z ${_ipaddr} ]]; then
  echo "Error: Could not retrieve IP Address."
  exit 1
fi

[[ ! -d ./inventory ]] \
  && mkdir ./inventory

echo "IP Address: ${_ipaddr}"
echo "Inventory: ${_inventory}"
echo
echo "[${_distro}_${_rand}]"                  > ${_inventory} 
echo "${_ipaddr}"                            >> ${_inventory} 
echo                                         >> ${_inventory} 
echo "[${_distro}_${_rand}:vars]"            >> ${_inventory} 
echo "ansible_user='root'"                   >> ${_inventory} 
echo "ansible_password='changeme'"           >> ${_inventory} 
echo "ansible_python_interpreter=${_python}" >> ${_inventory} 
echo "ansible_become='no'"                   >> ${_inventory} 
echo                                         >> ${_inventory} 


#####################################################
# Wait for SSH deamon
#####################################################

echo -e "\e[96m[Waiting for SSH damon]\e[39m"
waitSSH ${_ipaddr}

# Ensure removal of existing fingerprint
ssh-keygen \
  -f ~/.ssh/known_hosts \
  -R "${_ipaddr}" 2> /dev/null

echo

#####################################################
# Secure password
#####################################################

_playbook="password.yml"
if [[ -n `echo ${_sudo} | grep -i y` ]]; then
  _playbook="sudo.yml"
fi

echo -e "\e[96m[Changing Default Password]\e[39m"

_newpass=`pwgen -1s ${_pwlen}`

ansible-playbook \
  --extra-vars "newpassword=${_newpass} newuser=ans${_rand}" \
  -i ${_inventory} \
  ./playbooks/${_playbook}

[[ $? != 0 ]] && exit 1

sed -i "s/ansible_password=.*/ansible_password='${_newpass}'/g" ${_inventory}

if [[ ${_playbook} == "sudo.yml" ]]; then
  sed -i "s/ansible_user=.*/ansible_user='ansi${_rand}'/g" ${_inventory}
  sed -i "s/ansible_become=.*/ansible_become='yes'/g" ${_inventory}
fi


#####################################################
# Install default packages
#####################################################

echo -e "\e[96m[Running Playbooks]\e[39m"

ansible-playbook -i ${_inventory} ./playbooks/packages.yml


#####################################################
# Set hostname
#####################################################

_playbook="hostname.yml"
_hostname=$(basename ${_inventory} | cut -d. -f1)

ansible-playbook \
  --extra-vars "hostname=${_hostname}" \
  -i ${_inventory} \
  ./playbooks/${_playbook}


#####################################################
# Add non-root user
#####################################################

if [[ -n `echo ${_user}` ]]; then

  _playbook="user.yml"

  ansible-playbook \
    --extra-vars "user=${_user} newpassword=${_newpass} shell=${_shell}" \
    -i ${_inventory} \
    ./playbooks/${_playbook}
else
  _user="root"
fi


#####################################################
# Add to local SSH config
#####################################################

if [[ -n `echo ${_authkey} | grep -i y` ]]; then

  echo -e "\e[96m[Uploading SSH public key]\e[39m"
  ansible-playbook -i ${_inventory} ./playbooks/authkey.yml

  echo -e "\e[96m[Updating SSH configs]\e[39m"
  echo "Host ${_distro}-${_rand}" >> ~/.ssh/config
  echo "  Hostname ${_ipaddr}"    >> ~/.ssh/config
  echo "  User ${_user}"          >> ~/.ssh/config
  echo                            >> ~/.ssh/config
  echo "Host ${_distro}-${_rand} has been added to ~/.ssh/config"
  echo
  echo "Run \"ssh ${_distro}-${_rand}\" to login."
  echo
fi


#####################################################
# Exit
#####################################################

exit 0


