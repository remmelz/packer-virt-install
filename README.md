# packer-virt-install

Use packer to build a new template image and import it into kvm/libvirt.


## Requirements

The following applications are needed:

* ansible
* packer
* libvirt
* virt-install
* pwgen

## Download ISO images

To start building, the installation process requires the right dvd image
to be downloaded and copied into the distro iso folder. In the distro json file
you can set the image and the hash check. This can be sha256 or md5.

Example of ./archlinux/archlinux.json:

```
"iso_url": "./archlinux/iso/archlinux-2022.06.01-x86_64.iso",
"iso_checksum": "sha256:6b3bfe8d4e0d0f82cc3322f9565e92b0c44f27105889a665a8626ce47fbf7ab8",
```

## Getting started

Example for building a image and import it into Libvirt. The default password
is "changeme". After the installation, an inventory file is created so that
ansible can change the default password to a secure 24 characters long
password.

```
# ./build.sh rockylinux/rockylinux8.json

[Running Packer]
qemu: output will be in this color.

==> qemu: Retrieving ISO
==> qemu: Trying ./rockylinux/iso/Rocky-8.6-x86_64-dvd1.iso
==> qemu: Trying ./rockylinux/iso/Rocky-8.6-x86_64-dvd1.iso?checksum=md5%3A43ae6f2f80be76840ee9428f2ffbfbc1
==> qemu: ./rockylinux/iso/Rocky-8.6-x86_64-dvd1.iso?checksum=md5%3A43ae6f2f80be76840ee9428f2ffbfbc1 => /home/remmelz/Projects/remmelz/packer-virt-install/rockylinux/iso/Rocky-8.6-x86_64-dvd1.iso
==> qemu: Starting HTTP server on port 8519
    qemu: No communicator is set; skipping port forwarding setup.
==> qemu: Looking for available port between 5900 and 6000 on 127.0.0.1
==> qemu: Starting VM, booting from CD-ROM
==> qemu: Overriding default Qemu arguments with qemuargs template option...
==> qemu: Waiting 3s for boot...
==> qemu: Connecting to VM via VNC (127.0.0.1:5919)
==> qemu: Typing the boot command over VNC...
    qemu: No communicator is configured -- skipping StepWaitGuestAddress
==> qemu: Waiting for shutdown...
==> qemu: Converting hard drive...
Build 'qemu' finished after 15 minutes 42 seconds.

==> Wait completed after 15 minutes 42 seconds

==> Builds finished. The artifacts of successful builds are:
--> qemu: VM files in directory: output
renamed './output/rockylinux8.qcow2' -> './rockylinux/images/rockylinux8.qcow2'
Found image rockylinux8.qcow2...

[Creating Virtual Host]
'./rockylinux/images/rockylinux8.qcow2' -> '/var/lib/libvirt/images/rockylinux-p32m.qcow2'

Starting install...
Domain creation completed.
[Creating Inventory File]
IP Address: 192.168.122.33
Inventory: ./inventory/rockylinux-p32m.ini

[Wait for SSH]
SSH-2.0-OpenSSH_8.0

[Secure Password]
PLAY [all] ***************************************************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************************
ok: [192.168.122.33]

TASK [Change root password] **********************************************************************************************************************************************
changed: [192.168.122.33]

PLAY RECAP ***************************************************************************************************************************************************************
192.168.122.33             : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   


```


## Special keys
There are a set of special keys available. If these are in your boot command,
they will be replaced by the proper key:

    <bs> - Backspace

    <del> - Delete

    <enter> <return> - Simulates an actual "enter" or "return" keypress.

    <esc> - Simulates pressing the escape key.

    <tab> - Simulates pressing the tab key.

    <f1> - <f12> - Simulates pressing a function key.

    <up> <down> <left> <right> - Simulates pressing an arrow key.

    <spacebar> - Simulates pressing the spacebar.

    <insert> - Simulates pressing the insert key.

    <home> <end> - Simulates pressing the home and end keys.

    <pageUp> <pageDown> - Simulates pressing the page up and page down keys.

    <menu> - Simulates pressing the Menu key.

    <leftAlt> <rightAlt> - Simulates pressing the alt key.

    <leftCtrl> <rightCtrl> - Simulates pressing the ctrl key.

    <leftShift> <rightShift> - Simulates pressing the shift key.

    <leftSuper> <rightSuper> - Simulates pressing the ⌘ or Windows key.

    <wait> <wait5> <wait10> - Adds a 1, 5 or 10 second pause before sending any
    additional keys. This is useful if you have to generally wait for the UI to
    update before typing more.

    <waitXX> - Add an arbitrary pause before sending any additional keys. The
    format of XX is a sequence of positive decimal numbers, each with optional
    fraction and a unit suffix, such as 300ms, 1.5h or 2h45m. Valid time units
    are ns, us (or µs), ms, s, m, h. For example <wait10m> or <wait1m20s>.

    <XXXOn> <XXXOff> - Any printable keyboard character, and of these "special"
    expressions, with the exception of the <wait> types, can also be toggled on
    or off. For example, to simulate ctrl+c, use <leftCtrlOn>c<leftCtrlOff>. Be
    sure to release them, otherwise they will be held down until the machine
    reboots. To hold the c key down, you would use <cOn>. Likewise, <cOff> to
    release.

    {{ .HTTPIP }} {{ .HTTPPort }} - The IP and port, respectively of an HTTP
    server that is started serving the directory specified by the
    http_directory configuration parameter. If http_directory isn't specified,
    these will be blank!

    {{ .Name }} - The name of the VM.


